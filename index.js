const Matching = require(`./bin/matching`)
const config = require('./bin/config')
const cleanUpNeedsRemapCodes = require('./bin/needsRemapCodes')
const generateSourceMap = require('./bin/generateSourceMap')
const createMapFile = require('./bin/createMapFile')
const addHMaptoSrc = require('./bin/addHMaptoSrc')

const ALGORITHMSUSED = {
    EXACT_HASH_MAP: true,
    SAME_SIZE_DUPLICATE_BLOCKS: true,
    HASH_AROUND_BLOCKS: true,
    FIND_AND_FILL_HOLES: true,
}

//prompt for map input config
let SOURCEMAP={},
    TARGETMAP={},
    combinedFinalMap = {}

/**
 * Resets map values between sequential and before regular uses of the algorithm
 */
function initMaps(){
    //final map initialized with static opcodes
    combinedFinalMap = {
        "I_TELEPORT": 0,
        "I_SPAWN_BARRACK_CHARACTER": 1,
        "I_GFX_COMMAND": 17,
        "I_GFX_ADMIN_COMMAND": 18,
        "I_INPUT_COMMAND": 19,
        "I_MOVE_KEY": 20,
        "I_SKILL_KEY": 21,
        "I_CLOSE_SERVER_CONNECTION": 22,
        "C_CHECK_VERSION": 19900,
        "S_CHECK_VERSION": 19901
    }
    //reset SOURCE and TARGET maps
    SOURCEMAP={}
    TARGETMAP={}
}

/**
 * Awaits user configuration prompt then executes the matching algorithm for that configuration
 */
async function waitConfigure(){
    console.log("Algorithm Game Client Version Selection. Can generate new target mappings for version using ghidra scripts.")
    let configuration
    try {
        configuration = await config()
    } catch (e){
        console.error(e);
        process.exit(1);
    }
    if (configuration.length > 1){
        console.log(`Executing sequential run on versions: ${configuration.map(e=>e.targetVersion.match(/\d*\.\d*\w*/)[0]).join(" -> ")}`)
        for(let config of configuration){
            initMaps()
            console.log("=======================================================================")
            executeAlgorithm(config)
        }
    } else {
        initMaps()
        executeAlgorithm(configuration[0])
    }
}
function executeAlgorithm(configuration){
    console.log(`Configuration Set!\nSource Mapping: ${configuration.sourceVersion}\nTarget Mapping: ${configuration.targetVersion}`)
    if (!configuration.tboxCodes) console.log("No toolbox codes found for this patch version! This may affect the accuracy of the algorithm.")
    if (!configuration.handMapped) console.log("No handmapped codes found for target version.")
    //try to find handmapped for sourceversion
    let srcHandmapped = false
    try {
        srcHandmapped = require(`./tera-data-matching/handmapped/handmapped(${configuration.sourceVersion.match(/\d*\.\d*\w*/)[0]}).json`)
    } catch {
        console.log("No handmapped codes found for source version.")
    }
    SOURCEMAP = addHMaptoSrc(require(`./ghidramaps/${configuration.sourceVersion}`), srcHandmapped)
    TARGETMAP = require(`./ghidramaps/${configuration.targetVersion}`)
    //add know good codes
    if(configuration.tboxCodes) Object.assign(combinedFinalMap, require(`./tera-data-matching/tboxcodes/${configuration.tboxCodes}`))
    if(configuration.handMapped) Object.assign(combinedFinalMap, require(`./tera-data-matching/handmapped/${configuration.handMapped}`))
    
    //counting backwards since the most accurate mappings come last (for compatibility)
    for (let i = SOURCEMAP.length-1; i >= 0; i--) {
        //initialize datasets
        let matching = new Matching(SOURCEMAP[i], TARGETMAP[i], combinedFinalMap)
        //call the algorithms
        matching.matchMaps(ALGORITHMSUSED)
        combinedFinalMap = Object.assign(combinedFinalMap, matching.finalMap)
    }
    //add remap codes
    let { finalMap, needsRemapCount } = cleanUpNeedsRemapCodes(TARGETMAP, combinedFinalMap)
    let totalCodes = Object.keys(combinedFinalMap).length
    let validCodes = totalCodes - needsRemapCount
    console.log(`Valid Opcodes Mapped: ${validCodes} of ${totalCodes} (${Math.round((validCodes / totalCodes) * 100)}%)`)

    //create new source map
    generateSourceMap(configuration.targetVersion, finalMap)

    //create map file
    createMapFile(combinedFinalMap, configuration.ShareRevision)
    //get shared revision
    
}
waitConfigure();