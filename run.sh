#!/bin/bash
cd /app
log (){ echo "[Setup]: $1"; }
log "Reading client info from tera-client-dump."
curl --header "PRIVATE-TOKEN: $CI_REGISTRY_ACCESS_TOKEN" "https://gitlab.com/api/v4/projects/32061445/repository/files/ReleaseRevision%2Etxt/raw?ref=master" > ReleaseRevision.txt

patchVersion=$(cat ReleaseRevision.txt | \
grep "Version:" | \
grep -o "[[:digit:]]*\.[[:digit:]]*[[:blank:]][[:alpha:]]*" | \
sed "s/[[:blank:]]//g")

shareRevision=$(cat ReleaseRevision.txt | \
grep "ShareRevision: " | \
grep -o "[[:digit:]]*")

rm -rf ReleaseRevision.txt
log "Patch Version: $patchVersion"
log "Protocol Version: $shareRevision"

#the CI will be running in this repo so probably don't need to clone it
#well actually do need to commit source map here so maybe not
git clone https://oauth2:$CI_REGISTRY_ACCESS_TOKEN@gitlab.com/noctenium/update-pipeline/ghidramaps.git/

#clone tera-data-matching
git clone https://kloct:$GITHUB_ACCESS_TOKEN@github.com/kloct/tera-data-matching.git

#Run script
node . $patchVersion $shareRevision

#git config
git config --global user.name "TERA Heuristics CI"
git config --global user.email "Upfrontsuperior@gmail.com"

#Commit message
commitVersion=""
if [[ $SOURCE_VERSION != 'none' ]] && [[ $TARGET_VERSION != 'none' ]]; then
    commitVersion="Updated from $SOURCE_VERSION to $TARGET_VERSION sequentially"
else
    commitVersion="Updated to $patchVersion($shareRevision)"
fi
#commit output to tera-data-matching
cd tera-data-matching
git add .
git commit -m "$commitVersion"
git push
cd ..

#commit new source map to ghidramaps but don't rerun the pipeline
cd ghidramaps
git add .
git commit -m "$commitVersion"
git push -o ci.skip
cd ..