

const exactHashMap = require('./algorithms/exactHashMatch')
const sameSizeDuplicateBlocks = require('./algorithms/sameSizeDuplicateBlocks')
const hashAroundBlocks = require('./algorithms/hashAroundBlocks')
const findAndFillHoles = require('./algorithms/findAndFillHoles')
//some array field const enums
const ADDRESS_INDEX = 0,
    HASH_INDEX = 1,
    STRING_INDEX = 2

/** Class containing the framework for matching function signatures and relative memory addresses to identify opcodes */
module.exports = class Matching {
    /**Create a local instance of all of the mappings and lookup tables so they can be reused by the algorithms
     * @param {Array} sourceMap string -> signature/address mapping
     * @param {Array} targetMap opcode -> signature/address mapping
     */
    constructor(sourceMap, targetMap, combinedFinalMap) {
        //create utility maps
        //create copies of the input arrays so we don't accidently modify them
        this.sourceMap = sourceMap
        this.targetMap = targetMap

        //lookup tables (key value pair for hashes)
        //used to quickly check the existance of values in the source
        this.sourceHashLookupTable = this.#buildLookupTable(sourceMap, HASH_INDEX, STRING_INDEX)
        this.targetHashLookupTable = this.#buildLookupTable(targetMap, HASH_INDEX, STRING_INDEX)

        //arrays of only hashes
        this.sourceOnlyHashes = this.sourceMap.map(e => e[1])
        this.targetOnlyHashes = this.targetMap.map(e => e[1])

        //Array of only duplicate function hashes
        //Used for identifying blocks and deduplification
        this.duplicatesSource = this.#isolateDuplicates(this.sourceOnlyHashes)
        this.duplicatesTarget = this.#isolateDuplicates(this.targetOnlyHashes)

        //identifies repeating fn sig blocks and tags them with a range
        this.sourceBlockInfo = this.#generateDuplicateBlockInfo(this.sourceOnlyHashes)
        this.targetBlockInfo = this.#generateDuplicateBlockInfo(this.targetOnlyHashes)

        //initialize mapping with already found values
        this.finalMap = combinedFinalMap
        this.addedCodes = 0
    }
    //lookup tables builders
    #buildLookupTable(mapping, keyIndex, valueIndex) {
        let lookupTable = {}
        for (let code of mapping) lookupTable[code[keyIndex]] = code[valueIndex]
        return lookupTable
    }
    #isolateDuplicates(hashOnly) {
        let repeatedHashes = []
        for (let hash of hashOnly)
            if (hashOnly.filter(h => h === hash).length > 1)
                repeatedHashes.push(hash)
        return repeatedHashes
    }
    #generateDuplicateBlockInfo(source) {
        let blockInfo = {}
        let isRepeating = false
        let repeatingStart = 0
        for (let i = 0; i < source.length; i++) {
            if (source[i] === source[i + 1]) {
                isRepeating = true
                repeatingStart = i
                continue;
            } else if (isRepeating) {
                isRepeating = false
                if (!blockInfo[source[i]]) blockInfo[source[i]] = []
                blockInfo[source[i]].push([repeatingStart, i])
            }
        }
        return blockInfo
    }

    /**
     * adds found codes uniquely to final mapping
     * @param {Array} codes 
     */
    addCodes(codes) {
        for (let code in codes) {
            if (!this.finalMap[code]) { // check that opcode string not aready mapped
                if (Object.values(this.finalMap).indexOf(codes[code]) == -1) {//check that code doesn't already exist
                    this.finalMap[code] = codes[code]
                    this.addedCodes++
                }
            }
        }
    }
    /**
     * call the algorithms
     * @param {Array} algorithms 
     */
    matchMaps(algorithms) {
        //handle algorithm selection

        //call algorithm
        //bind this so it has access to class members
        if (algorithms.EXACT_HASH_MAP) exactHashMap.bind(this)()
        if (algorithms.SAME_SIZE_DUPLICATE_BLOCKS) sameSizeDuplicateBlocks.bind(this)()
        if (algorithms.HASH_AROUND_BLOCKS) hashAroundBlocks.bind(this)()
        if (algorithms.FIND_AND_FILL_HOLES) findAndFillHoles.bind(this)()
        return this.addedCodes
    }

    /**Returns an array of size from start
     * @param size size of the array
     * @param startAt the start of the array (0th index)
     */
    range(size, startAt = 0) {
        return [...Array(size).keys()].map(i => i + startAt);
    }

    getTest() {
        return this.sourceMap
    }
}