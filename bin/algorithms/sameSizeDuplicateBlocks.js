/** Checks if blocks of the same hashes are the same size and bounded by the same hashes
 * @returns true or false
*/
function validBlockMatch(sourceBounds, targetBounds) {
    if (sourceBounds && targetBounds) { //check if hashes for blocks match
        let sourcediff = sourceBounds[1] - targetBounds[0],
            targetdiff = sourceBounds[1] - targetBounds[0],
            sourceHashBeforeBlock = this.sourceOnlyHashes[sourceBounds[0] - 1],
            sourceHashAfterBlock = this.sourceOnlyHashes[sourceBounds[1] + 1],
            targetHashBeforeBlock = this.targetOnlyHashes[targetBounds[0] - 1],
            targetHashAfterBlock = this.targetOnlyHashes[targetBounds[1] + 1]
        //check if block lengths are the same
        //and check if upper and lower block bounds match hashes
        //(TODO): if bounds are mapped by previous algorithm pass
        if ((sourcediff == targetdiff) && ((sourceHashBeforeBlock === targetHashBeforeBlock) && (sourceHashAfterBlock === targetHashAfterBlock))) return true
    }
    return false
}

/**Creates mappings for blocks of the same size and bounding hashes from source to target */
module.exports = function mapSameSizeDuplicateBlocks() {
    for (let key of Object.keys(this.targetBlockInfo)) {
        if (this.sourceBlockInfo[key]) {
            for (let i = 0; i < this.sourceBlockInfo[key].length; i++) {
                //this checks if the block size for this index of repeated block matches the index of the source
                //also checks hash match above and below block ([start, end])
                if (validBlockMatch.bind(this)(this.sourceBlockInfo[key][i], this.targetBlockInfo[key][i])) {
                    //for every index in bounds
                    let sourceBlockRange = this.range(this.sourceBlockInfo[key][i].length, this.sourceBlockInfo[key][i][0]),
                        targetBlockRange = this.range(this.targetBlockInfo[key][i].length, this.targetBlockInfo[key][i][0])
                    //iterate over ranges and assign opcodes
                    for (let j = 0; j < sourceBlockRange.length; j++) {
                        let sourceMapString = this.sourceMap[sourceBlockRange[j]][2],
                            targetMapString = this.targetMap[targetBlockRange[j]][2]
                        this.addCodes({ [sourceMapString]: parseInt(targetMapString) })
                    }
                }
            }
        }
    }
}