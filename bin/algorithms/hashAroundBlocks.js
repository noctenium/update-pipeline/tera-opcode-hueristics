/**Counts the occurances of repeating blocks */
function countBlocks(source) {
    let blockCount = {};
    for (hash in source)
        blockCount[hash] = source[hash].length
    return blockCount
}

/**Finds block matches between source and target if they occur the same number of times */
function sameBlockOccurance(source, target) {
    let blockOccuranceMatch = {}
    for (let hash in source) {
        if (source[hash] == target[hash])
            blockOccuranceMatch[hash] = this.sourceBlockInfo[hash]
    }
    return blockOccuranceMatch
}

/**Finds matches around blocks of hashes of the same length that occur the same number of times */
module.exports = function hashAroundBlocks() {
    let sourceBlockCount = countBlocks(this.sourceBlockInfo),
        targetBlockCount = countBlocks(this.targetBlockInfo),
        blockOccuranceMatch = sameBlockOccurance.bind(this)(sourceBlockCount, targetBlockCount)
    //for every hash
    for (let hash in blockOccuranceMatch) {
        //for every block
        let currentBlocks = blockOccuranceMatch[hash]
        for (let i = 0; i < currentBlocks.length; i++) {
            //if is first or last hash continue
            if (currentBlocks[i][0] == 0 || currentBlocks[i][1] >= this.sourceMap.length || currentBlocks[i][1] >= this.targetMap.length)
                continue

            //get source and target preceeding hash
            let sourcePreBlockHash = this.sourceMap[currentBlocks[i][0] - 1][1]
            let targetPreBlockHash = this.targetMap[currentBlocks[i][0] - 1][1]
            //try to map (addCodes will reject if the code is already mapped)
            this.addCodes({ [this.sourceHashLookupTable[sourcePreBlockHash]]: parseInt(this.targetHashLookupTable[targetPreBlockHash]) })

            //get source and target post hash
            let sourcePostBlockHash = this.sourceMap[currentBlocks[i][1] + 1][1]
            let targetPostBlockHash = this.targetMap[currentBlocks[i][1] + 1][1]
            //try to map (addCodes will reject if the code is already mapped)
            this.addCodes({ [this.sourceHashLookupTable[sourcePostBlockHash]]: parseInt(this.targetHashLookupTable[targetPostBlockHash]) })
        }
    }
}