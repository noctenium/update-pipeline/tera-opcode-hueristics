/**
 * Match exact hash of all functions that occur only once.
 * This is the most accurate type of match.
 */

module.exports = function mapExactHashMatch() {
    function removeDuplicates(duplicates, mapping) {
        for (let hash of duplicates) {
            mapping.splice(mapping.map(e => e[1]).indexOf(hash), 1)
        }
        return mapping
    }

    let targetNoDuplicates = removeDuplicates(this.duplicatesTarget, [...this.targetMap]) //remove duplicates

    for (let code of targetNoDuplicates) {
        if (this.sourceHashLookupTable[code[1]])
            this.addCodes({ [this.sourceHashLookupTable[code[1]]]: parseInt(code[2]) })
    }
}