/**Looks at the size of the gaps between two mapped opcodes in the final map.
 * If the gap between these codes is the same from the source to the target then fills in the gap with what codes are usually there.
 * (It is important to run this algorithm last!)*/
module.exports = function findAndFillHoles() {
    //search targetMap for missing code gaps
    let mappedCodes = Object.values(this.finalMap),
        unmappedCodes = this.targetMap.map(e => parseInt(e[2])),
        sourceMapStrings = this.sourceMap.map(e => e[2]),
        currentGap = [],
        inGap = false,
        intermediateMap = {}
    //for all unmapped opcodes
    for (let i = 0; i < unmappedCodes.length; i++) {
        //is code assigned
        if (mappedCodes.indexOf(unmappedCodes[i]) == -1 && !inGap) {
            //code unmapped start counting
            currentGap.push(Object.keys(this.finalMap)[Object.values(this.finalMap).indexOf(unmappedCodes[i - 1])])
            currentGap.push(i)
            inGap = true

        } else if (mappedCodes.indexOf(unmappedCodes[i]) != -1 && inGap) { //reached the end of gap

            //find the same gap in source
            let startIndexSource = sourceMapStrings.indexOf(currentGap[0]),
                endIndexSource = sourceMapStrings.indexOf(Object.keys(this.finalMap)[Object.values(this.finalMap).indexOf(unmappedCodes[i])]),
                gapSizeSource = endIndexSource - startIndexSource - 1,
                gapSizeTarget = i - currentGap[1]
            //does gap size match source
            if (gapSizeSource == gapSizeTarget/*&&gapSizeTarget<6*/) {//large gaps reduce precision
                //fill gap
                let tempMap = {}
                for (let j = 0; j < gapSizeTarget; j++) {
                    //if gap contains a code that is already mapped abort block to prevent remap
                    //this happens if opcode is being remapped from a higher priority algorithm to fill this hole
                    //probably need to adjust the algorithm to account for this kind of stuff later
                    if (this.finalMap[this.sourceMap[(startIndexSource + j + 1)][2]]) {
                        tempMap = {}
                        break;
                    }
                    tempMap[this.sourceMap[(startIndexSource + j + 1)][2]] = parseInt(this.targetMap[currentGap[1] + j][2])
                }
                intermediateMap = Object.assign(intermediateMap, tempMap)
            }
            //reset vars
            inGap = false
            currentGap = []
        }
    }
    this.addCodes(intermediateMap)
}