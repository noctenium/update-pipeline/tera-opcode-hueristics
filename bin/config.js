const fs = require('fs');
const { SourceMap } = require('module');
const path = require('path');
const getToolboxCodes = require('./getTboxCodes')
const readline = require('readline'),
    rl = readline.createInterface({ input: process.stdin, output: process.stdout })
const FORMAT = {
    COLUMN_LENGTH: 15,
    ROW_COUNT: 5
}
//Questions
function question(q) { return new Promise(resolve => { rl.question(q, resolve) }) }
//Protocol Verions
let protoVer = require("../tera-data-matching/protoVer.json")
//list avalible game versions
let files = fs.readdirSync(path.join(__dirname, "../ghidramaps"), "ascii").filter((f)=>f.includes("MapSource")||f.includes("MapTarget"))
let versions = [... new Set(files.map(file => file.match(/\(([^()]+)\)/)[1]))]
versions.sort((a, b)=> parseInt(a.match(/\d*\.?\d*/))-parseInt(b.match(/\d*\.?\d*/)))
//make version list
let list = ""
for (let i = 0; i < versions.length; i++) {
    if ((i + 1) % (Math.round(versions.length / FORMAT.ROW_COUNT)) != 0) {
        list += (`${padding(`[${i}] ${versions[i]}`)} `)
    } else {
        list += (`${padding(`[${i}] ${versions[i]}`)} \n`)
    }
}
/**
 * Adds Padding to strings to make them all the same length (COLUMN_LENGTH)
 * @param {string} str 
 * @returns formatted string
 */
function padding(str) {
    if (str.length < FORMAT.COLUMN_LENGTH) {
        for (let i = str.length; i < FORMAT.COLUMN_LENGTH; i++) {
            str += " ";
        }
        return str
    } else {
        return str.slice(0, FORMAT.COLUMN_LENGTH)
    }
}

//this is where the final version info is stored
/**
 * Checks to see if input is valid
 * @param {string} selection 
 * @returns object containing selected indexes
 */
function validateVersionSelect(selection){
    let selectedVersionI = selection.split(" ")
    let sourceVerI, targetVerI
    if(selectedVersionI.length == 2){
        sourceVerI = parseInt(selectedVersionI[0])
        targetVerI = parseInt(selectedVersionI[1])
        if (Number.isNaN(sourceVerI)|| Number.isNaN(targetVerI)){
            return invalidSelection("Invalid selection! Selection is not a number.");
        } else if(sourceVerI > versions.length||targetVerI > versions.length){
            return invalidSelection("Invalid selection! Selection out of bounds.")
        } else {
            let selectedVersions = selectVersion(sourceVerI, targetVerI)
            if (!selectedVersions.sourceVersion){
                return invalidSelection(`No source map exists for version: ${versions[sourceVerI]}`)
            } else if (!selectedVersions.targetVersion){
                return invalidSelection(`No target map exists for version: ${versions[targetVerI]}`)
            } else{
                return selectedVersions
            }
        }
    } else invalidSelection("Invalid selection! Only select 2 versions separated by a space.")
}
function invalidSelection(msg){
    console.log(msg)
    return false
}

/**
 * Given the version selection indexes, Fetches associated files for that version if they are avalible.
 * @param {Integer} sourceIndex 
 * @param {Integer} targetIndex 
 * @returns fetched file names if avalible, false if not
 */
function selectVersion(sourceIndex, targetIndex) {
    let sourceVersion, targetVersion, tboxCodes, handMapped
    sourceVersion = `MapSource(${versions[sourceIndex]}).json`
    sourceVersion = fs.existsSync(path.join(__dirname, `../ghidramaps/${sourceVersion}`)) ? sourceVersion : false
    targetVersion = `MapTarget(${versions[targetIndex]}).json`
    targetVersion = fs.existsSync(path.join(__dirname, `../ghidramaps/${targetVersion}`)) ? targetVersion : false
    tboxCodes = `tboxcodes(${versions[targetIndex]}).json`
    tboxCodes = fs.existsSync(path.join(__dirname, `../tera-data-matching/tboxCodes/${tboxCodes}`)) ? tboxCodes : false
    handMapped = `handmapped(${versions[targetIndex]}).json`
    handMapped = fs.existsSync(path.join(__dirname, `../tera-data-matching/handmapped/${handMapped}`)) ? handMapped : false
    return [{ sourceVersion, targetVersion, tboxCodes, handMapped }]
}

function getHandMapped(targetVersion){
    return fs.existsSync(path.join(__dirname, `../tera-data-matching/handmapped/handmapped(${targetVersion}).json`))? `handmapped(${targetVersion}).json`: false
}

/**
 * Automatically run algorithm with configuration passed by arguments
 * @returns configuration object
 */
async function argumentConfig(){
    let configuration={targetVersion:"", sourceVersion:"", tboxCodes: false, handMapped: false, ShareRevision: ""}

    configuration.ShareRevision = process.argv[3]
    let targetVersion = process.argv[2]
    configuration.targetVersion=`MapTarget(${targetVersion}).json`
    // update protoVer.json
    protoVer[targetVersion] = configuration.ShareRevision
    fs.writeFileSync(path.join(__dirname, "../tera-data-matching/protoVer.json"), JSON.stringify(protoVer, null, 1))
    // Set sourceVersion as latest previous version
    let sourceMaps = fs.readdirSync("ghidramaps").filter((f)=>f.includes("MapSource"))
    if (!sourceMaps.includes(`MapSource(${targetVersion}).json`)){
        sourceMaps.push(`MapSource(${targetVersion}).json`)//figure out where it would have gone to find previous version
    }
    //sort everthing
    sourceMaps.sort((a, b)=>parseFloat(a.match(/\d*\.\d*/)[0])-parseFloat(b.match(/\d*\.\d*/)[0]))
    //Preferentially select GF source of same version as target to xreference unless it's referencing itself
    for (let map of sourceMaps){
        if (map.includes(`${targetVersion.substring(0, 6)}GF`)&&map.includes("GF")==-1)
            configuration.sourceVersion = map
    }
    if(configuration.sourceVersion.length==0){
        //try to select version that is the same as target
        for (let map of sourceMaps){
            if(map.includes(targetVersion))
                configuration.sourceVersion = map
        }
        //if target version is included as source select version before it
        configuration.sourceVersion = sourceMaps[sourceMaps.indexOf(sourceMaps.filter(f=>f.includes(targetVersion))[0])-1]

    }
    //try to fetch toolbox codes for this version and write to tera-data-matching for archiving
    let tboxCodes = await getToolboxCodes(configuration.ShareRevision)
    if (tboxCodes){
        fs.writeFileSync(path.join(__dirname, `../tera-data-matching/tboxcodes/tboxcodes(${targetVersion}).json`), JSON.stringify(tboxCodes, null, 1))
        configuration.tboxCodes = `tboxcodes(${targetVersion}).json`
    }
    configuration.handMapped = getHandMapped(targetVersion)
    return [configuration]
}

/**
 * Gathers configuration from enviornment variables for executing the algorithm
 * @returns configuration object
 */
async function manualConfig(){
    if(process.env.TARGET_PROTO) protoVer[process.env.TARGET_VERSION] = process.env.TARGET_PROTO
    // check if versions are valid
    if(!versions.includes(process.env.TARGET_VERSION)) throw `Target Map for version "${process.env.TARGET_VERSION}" not found! (${versions.concat(", ")})`
    if(!versions.includes(process.env.SOURCE_VERSION)) throw `Source Map for version "${process.env.TARGET_VERSION}" not found! (${versions.concat(", ")})`
    // returns array of all versions to run algorithm between source and target
    if(process.env.SEQUENTIAL == "true" && versions.indexOf(process.env.TARGET_VERSION)-versions.indexOf(process.env.SOURCE_VERSION)!=1){
        let configurationArr = []
        versions = versions.filter((v=>v.includes("GF"))) // removes all non GF versions from sequential running
        for(let i = versions.indexOf(process.env.SOURCE_VERSION); i<versions.indexOf(process.env.TARGET_VERSION); i++){
            configurationArr.push({
                targetVersion: `MapTarget(${versions[i+1]}).json`,
                sourceVersion: `MapSource(${versions[i]}).json`,
                tboxCodes: await getToolboxCodes(versions[i+1]),
                handMapped: getHandMapped(versions[i+1]),
                ShareRevision: protoVer[versions[i+1]]
            })
        }
        return configurationArr
    }
    if(!protoVer[process.env.TARGET_VERSION]&&!process.env.TARGET_PROTO) throw "No ShareRevision protocol version specified or found!"
    console.log("test")
    return [{
        targetVersion: `MapTarget(${process.env.TARGET_VERSION}).json`,
        sourceVersion: `MapSource(${process.env.SOURCE_VERSION}).json`,
        tboxCodes: await getToolboxCodes(process.env.TARGET_VERSION),
        handMapped: getHandMapped(process.env.TARGET_VERSION),
        ShareRevision: protoVer[process.env.TARGET_VERSION]
    }]

}

/**
 * Prompts user for mapping source and target version selection
 * @returns either the version info selected by the user or recursive call to the same function to ask the user again
 */
module.exports = async function askForVersion(){
    // Executes the algorithm with a custom source and target map
    if (process.env.SOURCE_VERSION!="none" && process.env.TARGET_VERSION!="none" && process.env.TARGET_VERSION && process.env.SOURCE_VERSION){ 
        rl.close()
        return manualConfig()
    }
    // The normal way the algorithm is executed in the pipeline
    else if (process.argv[2]&&process.argv[3]){ 
        rl.close()
        return argumentConfig()
    }
    console.log("Avalible Versions:")
    console.log(list);
    let responseVersions = await question(`Please select a source and target version. "source target": `)
    versionInfo = validateVersionSelect(responseVersions)
    if(!versionInfo) return await askForVersion();
    else {
        rl.close()
        return [versionInfo]
    }
}