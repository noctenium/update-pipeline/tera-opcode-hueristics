
//get unique list of all codes in target map marked as client or server codes
//for each code
//if it's not on the found codes list
//add it as a needs remap code
//increment needsRemap count
function reverseMap(mapping) {
    let remapped = {}
    for (map in mapping) {
        remapped[mapping[map]] = map
    }
    return remapped
}
module.exports = function cleanUpNeedsRemapCodes(TARGETMAP, combinedFinalMap) {
    //get a list of all codes
    let uniqueCodes = {},
        finalMap = combinedFinalMap,
        needsRemapCount = 0
    for (mapType of TARGETMAP) {
        for (code of mapType) {
            if (!uniqueCodes.code)
                uniqueCodes[code[2]] = code[3]
        }
    }
    let finalMapLookup = reverseMap(finalMap)
    //truncate remaped codes from finalMap
    for (code in finalMapLookup) {
        if (finalMapLookup[code].match("NEEDS_REMAP") != null) {
            delete finalMap[finalMapLookup[code]]
            delete finalMapLookup[code]
        }
    }
    //create REMAP mappings
    for (code in uniqueCodes) {
        let codeInt = parseInt(code)
        if (finalMapLookup[codeInt] == undefined) {
    
            finalMap[`${uniqueCodes[code] == "server" ? "SU" : uniqueCodes[code] == "client" ? "CU" : "UNK"}_NEEDS_REMAP_${codeInt.toString(16).toUpperCase()}`] = codeInt
            needsRemapCount++
        }
    }
    return {finalMap, needsRemapCount}
}