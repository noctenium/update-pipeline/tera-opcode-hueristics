/**
 * Adds handmapped codes to sourcemap since handmapped codes will be added after the source map was generated
 */
module.exports = function addHandmappedCodesToSourceMap(SOURCEMAP, handMappedSource){
    if (!handMappedSource){
        return SOURCEMAP
    }
    let sourceMapModded = SOURCEMAP,
        addedcodes = 0,
        reverseHandMapped = {}

    for (code in handMappedSource) reverseHandMapped[handMappedSource[code]] = code
    //search for applicable codes
    for(mapType of sourceMapModded){
        for(code of mapType){
            if (code[2].match("NEEDS_REMAP")){
                let opcode = parseInt(code[2].split("_")[3], 16)
                if (reverseHandMapped[opcode]){
                    addedcodes ++
                    code[2] = reverseHandMapped[opcode]
                }
            }
        }
    }
    console.log(`Added ${addedcodes} codes from handmapped to source.`)
    return sourceMapModded
}