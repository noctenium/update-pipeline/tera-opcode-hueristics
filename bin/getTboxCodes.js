const fs = require("fs")
const path = require("path")
const https = require("https")

module.exports = function getToolboxCodes(version) {
    if (!Number(version)) {
        return fs.existsSync(path.join(__dirname, `../tera-data-matching/tboxcodes/tboxcodes(${version}).json`)) ? `tboxcodes(${version}).json`: false
    }
    return new Promise((resolve, reject) => {
        const request = https.request("https://raw.githubusercontent.com/tera-toolbox/tera-toolbox/master/data/data.json", (res) => {
            let data = '';
            res.on('data', (chunk) => {
                data = data + chunk.toString();
            });
            res.on('end', () => {
                const body = JSON.parse(data);
                resolve(body.maps[version]?body.maps[version]:false)
            })
        })
        request.on('error', err => {
            reject(err)
        })
        request.end()
    })
}
