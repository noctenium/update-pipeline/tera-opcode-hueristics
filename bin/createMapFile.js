module.exports = function createMapFile(source, sharedRevision){
    const fs = require('fs')
    let sourceString = ""
    for (let code in source) {
        if (code != "")
            sourceString = sourceString + `${code} ${source[code]}\n`
    }
    //sort map
    let codes = sourceString.split("\n").sort(),
        mapString = ""
    for (let code of codes) {
        mapString += `${code}\n`
    }

    fs.writeFileSync(require('path').join(__dirname, `../tera-data-matching/maps/protocol.${sharedRevision}.map`), sourceString)
    console.log(`Saved Final Mapping to tera-data-matching/maps/protocol.${sharedRevision}.map`)
}

