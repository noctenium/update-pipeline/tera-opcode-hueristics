const path = require('path')
const fs = require('fs')
function reverseMap(mapping) {
    let remapped = {}
    for (map in mapping) {
        remapped[mapping[map]] = map
    }
    return remapped
}
module.exports = function CreateNewSourceMap(targetMapFile, foundCodes) {
    const MapTarget = require(`../ghidramaps/${targetMapFile}`),
        opcodeMap = foundCodes,
        targetVersion = targetMapFile.match(/\d*\.\d*\w*/)[0]
    let opcodeMapReverseLookup = reverseMap(opcodeMap),
        remappedTarget = []
    for (let i = 0; i < MapTarget.length; i++) {
        let remappedMapType = []
        for (code of MapTarget[i]) {
            remappedMapType.push([code[0], code[1], opcodeMapReverseLookup[code[2]], code[3]])
        }
        remappedTarget.push(remappedMapType)
    }
    fs.writeFileSync(path.join(__dirname, `../ghidramaps/MapSource(${targetVersion}).json`), JSON.stringify(remappedTarget, null, 1))
    console.log(`Generated source map for ${targetVersion}`)
}


