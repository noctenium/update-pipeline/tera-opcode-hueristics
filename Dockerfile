FROM node:17-alpine

#install dependencies
RUN apk --no-cache add curl git

ENV CI_REGISTRY_ACCESS_TOKEN ""
ENV GITHUB_ACCESS_TOKEN ""

COPY . /app

#no entrypoint (CI will execute run.sh) (/bin/sh /app/run.sh)